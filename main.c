int maxTime = 0; //Sekunden für das Leuchten der Pins

int taster1 = 2;    //taster 1 pin
int led1 = 5;       //led pin
int taster1State = 0; //state of the led
unsigned long timer1 = 0; //timer 

int taster2 = 3;
int led2 = 6;
int taster2State = 0;
unsigned long timer2 = 0;

int taster3 = 4;
int led3 = 7;
int taster3State = 0;
unsigned long timer3 = 0;

int summerPin = 8;
unsigned long summerTimer = 0;
int summerMaxTime = 1000; //wie lange der summer summt

int potentiometerPin = A1;

int convertPotentiometerToMaxTime(int value) {
  return value * 10;
}

void triggerSummer(int summerPin, unsigned long * timer) {
  if(*timer > 0) return; //summer bricht ab
  
  *timer = millis();
  tone(summerPin, 440); //summer starten
}

/**
 * der summer soll
 */
void checkSummerTimer() {
   if(summerTimer > 0) { 
    int timeDiff = millis() - summerTimer;
    if(timeDiff >= summerMaxTime) {
      noTone(summerPin);
      summerTimer =0;
    }
  }
}

void checkTaster(int inPin, int outPin, int * state, unsigned long* timer) {
  if(*timer > 0 ) {
      int timeDiff = millis() - *timer;
  
    if(timeDiff >= maxTime){
      *state = 0;
      *timer = 0; 
    }
  }

  checkSummerTimer();

  
  if (digitalRead(inPin) == LOW && * state == 0) {
    digitalWrite(outPin, LOW);
    * state = 1;
  } else {
    delay(10);
  }

  if (digitalRead(inPin) == HIGH && * state == 1) {
    digitalWrite(outPin, HIGH);
    triggerSummer(summerPin, &summerTimer);
    *timer = millis();
    * state = 2;
  } else {
    delay(10);
  }

  if (digitalRead(inPin) == LOW && * state == 2) {
    digitalWrite(outPin, HIGH);
    * state = 3;
  } else {
    delay(10);
  }

  if (digitalRead(inPin) == HIGH && * state == 3) {
    digitalWrite(outPin, LOW);
    * state = 0;
  } else {
    delay(10);
  }
}

void setup()
{
  pinMode(taster1, OUTPUT);   
  pinMode(led1, INPUT);

  pinMode(taster2, OUTPUT);
  pinMode(led2, INPUT);

  pinMode(taster3, OUTPUT);
  pinMode(led3, INPUT);
  Serial.begin(9600);
}

 void loop()   
{
  checkTaster(taster1, led1, &taster1State, &timer1);
  checkTaster(taster2, led2, &taster2State, &timer2);
  checkTaster(taster3, led3, &taster3State, &timer3);
  int potValue = analogRead(potentiometerPin);
  maxTime = convertPotentiometerToMaxTime(potValue);

 }

 
